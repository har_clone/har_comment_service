package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	GrpcPort                 string
	Postgres                 PostgresConfig
	AuthSecretKey            string
	ApartmentServiceGrpcPort string
	ApartmentServiceHost     string
	UserServiceGrpcPort      string
	UserServiceHost          string
	BrokerServiceGrpcPort    string
	BrokerServiceHost        string
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		GrpcPort: conf.GetString("GRPC_PORT"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},

		AuthSecretKey: conf.GetString("AUTH_SECRET_KEY"),

		UserServiceGrpcPort:      conf.GetString("USER_SERVICE_GRPC_PORT"),
		UserServiceHost:          conf.GetString("USER_SERVICE_HOST"),
		BrokerServiceGrpcPort:    conf.GetString("BROKER_SERVICE_GRPC_PORT"),
		BrokerServiceHost:        conf.GetString("BROKER_SERVICE_HOST"),
		ApartmentServiceGrpcPort: conf.GetString("APARTMENT_SERVICE_GRPC_PORT"),
		ApartmentServiceHost:     conf.GetString("APARTMENT_SERVICE_HOST"),
	}

	return cfg
}
