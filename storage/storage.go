package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_comment_service/storage/postgres"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
)

type StorageI interface {
	Comment() repo.CommentStorageI
	Like() repo.LikeStorageI
	CollectedApartments() repo.CollectedApartmentsStorageI
}

type storagePg struct {
	commentRepo             repo.CommentStorageI
	likeRepo                repo.LikeStorageI
	collectedApartmentsRepo repo.CollectedApartmentsStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		commentRepo:             postgres.NewComment(db),
		likeRepo:                postgres.NewLike(db),
		collectedApartmentsRepo: postgres.NewCollectedApartment(db),
	}
}

func (s *storagePg) Comment() repo.CommentStorageI {
	return s.commentRepo
}

func (s *storagePg) Like() repo.LikeStorageI {
	return s.likeRepo
}

func (s *storagePg) CollectedApartments() repo.CollectedApartmentsStorageI {
	return s.collectedApartmentsRepo
}
