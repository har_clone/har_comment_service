package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
)

type collectedApartmentRepo struct {
	db *sqlx.DB
}

func NewCollectedApartment(db *sqlx.DB) repo.CollectedApartmentsStorageI {
	return &collectedApartmentRepo{
		db: db,
	}
}

func (cr *collectedApartmentRepo) AddApartment(req *repo.AddApartmentReq) (*repo.CollectedApartment, error) {
	var result repo.CollectedApartment
	query := `
		INSERT INTO collected_apartments(
			user_id,
			apartment_id
		)values($1,$2)
		RETURNING
			id,
			user_id,
			apartment_id,
			created_at
	`
	row := cr.db.QueryRow(query, req.UserId, req.ApartmentId)
	if err := row.Scan(
		&result.Id,
		&result.UserId,
		&result.ApartmentId,
		&result.CreatedAt,
	); err != nil {
		return &repo.CollectedApartment{}, err
	}
	return &result, nil
}
func (cr *collectedApartmentRepo) GetApartment(Id int64) (*repo.CollectedApartment, error) {
	var result repo.CollectedApartment
	query := `
		SELECT 
			id,
			user_id,
			apartment_id,
			created_at
		FROM collected_apartments
		WHERE id=$1
	`
	row := cr.db.QueryRow(query, Id)
	if err := row.Scan(
		&result.Id,
		&result.UserId,
		&result.ApartmentId,
		&result.CreatedAt,
	); err != nil {
		return &repo.CollectedApartment{}, err
	}
	return &result, nil
}

func (cr *collectedApartmentRepo) GetAllCollectedApartments(param *repo.GetAllCollectedApartmentsReq) (*repo.GetAllCollectedApartmentsRes, error) {
	result := repo.GetAllCollectedApartmentsRes{
		Apartments: make([]*repo.CollectedApartment, 0),
	}

	offset := (param.Page - 1) * param.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", param.Limit, offset)

	filter := ""

	if filter == "" && param.UserId != 0 {
		filter = fmt.Sprintf(" where user_id=%d ", param.UserId)
	} else if filter != "" && param.UserId != 0 {
		filter += fmt.Sprintf(" and user_id=%d ", param.UserId)
	}

	if param.SortByDate == "" {
		param.SortByDate = "desc"
	}

	query := `
		SELECT
			id,
			user_id,
			apartment_id,
			created_at
		FROM collected_apartments
		` + filter + `
		ORDER BY created_at ` + param.SortByDate + ` ` + limit

	rows, err := cr.db.Query(query)
	if err != nil {
		return &repo.GetAllCollectedApartmentsRes{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var a repo.CollectedApartment
		err := rows.Scan(
			&a.Id,
			&a.UserId,
			&a.ApartmentId,
			&a.CreatedAt,
		)
		if err != nil {
			return &repo.GetAllCollectedApartmentsRes{}, err
		}
		result.Apartments = append(result.Apartments, &a)
	}

	queryCount := `SELECT count(1) FROM collected_apartments ` + filter
	err = cr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return &repo.GetAllCollectedApartmentsRes{}, err
	}
	return &result, nil
}

func (cr *collectedApartmentRepo) DeleteApartment(Id int64) error {
	effect, err := cr.db.Exec("DELETE FROM collected_apartments WHERE id=$1", Id)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}
