package postgres

import (
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
)

type likeRepo struct {
	db *sqlx.DB
}

func NewLike(db *sqlx.DB) repo.LikeStorageI {
	return &likeRepo{
		db: db,
	}
}

func (lr *likeRepo) CreateOrUpdate(l *repo.Like) error {
	like, err := lr.Get(&repo.GetLikeInfo{
		LikeType: l.LikeType,
		GuestId:  l.GuestId,
		UserId:   l.UserId,
	})
	if errors.Is(err, sql.ErrNoRows) {
		query := `
			INSERT INTO likes(user_id, like_type, guest_id, status) 
			VALUES($1, $2, $3, $4) RETURNING id
		`

		_, err := lr.db.Exec(query, l.UserId, l.LikeType, l.GuestId, l.Status)
		if err != nil {
			return err
		}
	} else if like != nil {
		if like.Status == l.Status {
			_, err := lr.db.Exec(`DELETE FROM likes WHERE id=$1`, like.Id)
			if err != nil {
				return err
			}
		} else {
			query := `UPDATE likes SET status=$1 WHERE id=$2`
			_, err := lr.db.Exec(query, l.Status, like.Id)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (lr *likeRepo) Get(info *repo.GetLikeInfo) (*repo.Like, error) {
	var result repo.Like

	query := `
		SELECT
			id,
			user_id,
			guest_id,
			like_type,
			status
		FROM likes
		WHERE user_id=$1 AND guest_id=$2 AND like_type=$3
	`

	row := lr.db.QueryRow(query, info.UserId, info.GuestId, info.LikeType)
	err := row.Scan(
		&result.Id,
		&result.UserId,
		&result.GuestId,
		&result.LikeType,
		&result.Status,
	)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &repo.Like{}, err
		}
		return &repo.Like{}, nil
	}

	return &result, nil
}

func (lr *likeRepo) GetLikesDislikesCount(req *repo.GetLikesDislikesCountReq) (*repo.LikesDislikesCountsResult, error) {
	var result repo.LikesDislikesCountsResult

	query := `
		SELECT
			COUNT(1) FILTER (WHERE status=true) as likes_count,
			COUNT(1) FILTER (WHERE status=false) as dislikes_count
		FROM likes
		WHERE guest_id=$1 AND like_type=$2
	`

	row := lr.db.QueryRow(query, req.Id, req.LikeType)
	err := row.Scan(
		&result.LikesCount,
		&result.DislikesCount,
	)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &repo.LikesDislikesCountsResult{}, err
		}
		return &repo.LikesDislikesCountsResult{}, nil
	}

	return &result, nil
}
