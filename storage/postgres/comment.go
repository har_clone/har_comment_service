package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/har_clone/har_comment_service/pkg/utils"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
)

type commentRepo struct {
	db *sqlx.DB
}

func NewComment(db *sqlx.DB) repo.CommentStorageI {
	return &commentRepo{
		db: db,
	}
}
func (cr *commentRepo) CreateComment(comment *repo.CommentCreate) (*repo.Comment, error) {
	var ResComment repo.Comment

	query := `
		INSERT INTO comments(
			comment_type,
			guest_id,
			user_id,
			description
		)values($1,$2,$3,$4)
		RETURNING 
			id,
			COALESCE(comment_type,'') as comment_type,
			COALESCE(guest_id,-1) as guest_id,
			COALESCE(user_id,-1) as user_id,
			COALESCE(description,'') as description,
			created_at
	`

	row := cr.db.QueryRow(
		query,
		comment.CommentType,
		comment.GuestId,
		comment.UserId,
		utils.NullString(comment.Description),
	)

	if err := row.Scan(
		&ResComment.Id,
		&ResComment.CommentType,
		&ResComment.GuestId,
		&ResComment.UserId,
		&ResComment.Description,
		&ResComment.CreatedAt,
	); err != nil {
		return &repo.Comment{}, err
	}

	return &ResComment, nil
}

func (cr *commentRepo) GetComment(Id int64) (*repo.Comment, error) {
	var ResponseComment repo.Comment

	query := `
		SELECT 
			id,
			comment_type,
			guest_id,
			user_id,
			COALESCE(description,'') as description,
			created_at
		FROM comments
		where id = $1
	`

	row := cr.db.QueryRow(query, Id)

	if err := row.Scan(
		&ResponseComment.Id,
		&ResponseComment.CommentType,
		&ResponseComment.GuestId,
		&ResponseComment.UserId,
		&ResponseComment.Description,
		&ResponseComment.CreatedAt,
	); err != nil {
		return &repo.Comment{}, err
	}

	return &ResponseComment, nil
}

func (cr *commentRepo) GetAllComments(param *repo.GetAllCommentRequest) (*repo.GetAllCommentsResponse, error) {
	Result := repo.GetAllCommentsResponse{
		Comments: make([]*repo.Comment, 0),
	}

	offset := (param.Page - 1) * param.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", param.Limit, offset)

	filter := " WHERE true "

	if param.Search != "" {
		str := "%" + param.Search + "%"
		filter += fmt.Sprintf(`
			and (description ILIKE '%s') `,
			str,
		)
	}
	if param.CommentType != "" {
		str := "%" + param.CommentType + "%"
		filter += fmt.Sprintf(`
			and (comment_type ILIKE '%s') `,
			str,
		)
	}

	if filter == "" && param.GuestId != 0 {
		filter = fmt.Sprintf(" where guest_id=%d ", param.GuestId)
	} else if filter != "" && param.GuestId != 0 {
		filter += fmt.Sprintf(" and guest_id=%d ", param.GuestId)
	}

	if filter == "" && param.UserId != 0 {
		filter = fmt.Sprintf(" where user_id=%d ", param.UserId)
	} else if filter != "" && param.UserId != 0 {
		filter += fmt.Sprintf(" and user_id=%d ", param.UserId)
	}

	if param.SortByDate == "" {
		param.SortByDate = "desc"
	}

	query := `
		SELECT
			id,
			comment_type,
			guest_id,
			user_id,
			COALESCE(description,'') as description,
			created_at
		FROM comments
		` + filter + `
		ORDER BY created_at ` + param.SortByDate + ` ` + limit

	rows, err := cr.db.Query(query)

	if err != nil {
		return &repo.GetAllCommentsResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var u repo.Comment

		err := rows.Scan(
			&u.Id,
			&u.CommentType,
			&u.GuestId,
			&u.UserId,
			&u.Description,
			&u.CreatedAt,
		)
		if err != nil {
			return &repo.GetAllCommentsResponse{}, err
		}

		Result.Comments = append(Result.Comments, &u)
	}

	queryCount := `SELECT count(1) FROM comments ` + filter
	err = cr.db.QueryRow(queryCount).Scan(&Result.Count)
	if err != nil {
		return &repo.GetAllCommentsResponse{}, err
	}

	return &Result, nil
}

func (cr *commentRepo) UpdateComment(comment *repo.ChangeComment) (*repo.Comment, error) {
	var ResponseComment repo.Comment
	query := `
		UPDATE comments SET 
			description=$1
		where id=$2
		RETURNING 
			id,
			comment_type,
			guest_id,
			user_id,
			COALESCE(description,'') as description,
			created_at
	`
	row := cr.db.QueryRow(
		query,
		comment.Description,
		comment.Id,
	)

	if err := row.Scan(
		&ResponseComment.Id,
		&ResponseComment.CommentType,
		&ResponseComment.GuestId,
		&ResponseComment.UserId,
		&ResponseComment.Description,
		&ResponseComment.CreatedAt,
	); err != nil {
		return &repo.Comment{}, err
	}

	return &ResponseComment, nil
}

func (cr *commentRepo) DeleteComment(ID int64) error {
	effect, err := cr.db.Exec("DELETE FROM comments WHERE id=$1", ID)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}
