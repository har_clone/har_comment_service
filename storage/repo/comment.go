package repo

import "time"

type Comment struct {
	Id          int64     `json:"id"`
	CommentType string    `json:"comment_type"`
	GuestId     int64     `json:"guest_id"`
	UserId      int64     `json:"user_id"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type CommentCreate struct {
	CommentType string `json:"comment_type"`
	GuestId     int64  `json:"guest_id"`
	UserId      int64  `json:"user_id"`
	Description string `json:"description"`
}

type ChangeComment struct {
	Id          int64
	Description string `json:"description"`
	UserId      int64
}

type GetAllCommentRequest struct {
	Page        int    `json:"page" db:"page" binding:"required" default:"1"`
	Limit       int    `json:"limit" db:"limit" binding:"required" default:"10"`
	GuestId     int    `json:"guest_id" db:"guest_id"`
	UserId      int    `json:"user_id" db:"user_id"`
	Search      string `json:"search"`
	CommentType string `json:"comment_type"`
	SortByDate  string `json:"sort_by_date" enums:"asc,desc" default:"desc"`
}

type GetAllCommentsResponse struct {
	Comments []*Comment `json:"comments"`
	Count    int        `json:"count"`
}

type UserInfo struct {
	Id              int64  `json:"id"`
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Username        string `json:"username"`
	ProfileImageUrl string `json:"profile_image_url"`
	Bio             string `json:"bio"`
	Email           string `json:"email"`
	Gender          string `json:"gender"`
	PhoneNumber     string `json:"phone_number"`
	Type            string `json:"type"`
}

type CommentStorageI interface {
	CreateComment(*CommentCreate) (*Comment, error)
	GetComment(int64) (*Comment, error)
	GetAllComments(*GetAllCommentRequest) (*GetAllCommentsResponse, error)
	UpdateComment(*ChangeComment) (*Comment, error)
	DeleteComment(int64) error
}
