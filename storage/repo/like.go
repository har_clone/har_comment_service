package repo

type Like struct {
	Id       int64
	LikeType string
	GuestId  int64
	UserId   int64
	Status   bool
}

type GetLikeInfo struct {
	LikeType string
	GuestId  int64
	UserId   int64
}

type LikesDislikesCountsResult struct {
	LikesCount    int64
	DislikesCount int64
}

type GetLikesDislikesCountReq struct {
	Id       int64
	LikeType string
}

type LikeStorageI interface {
	CreateOrUpdate(*Like) error
	Get(*GetLikeInfo) (*Like, error)
	GetLikesDislikesCount(*GetLikesDislikesCountReq) (*LikesDislikesCountsResult, error)
}
