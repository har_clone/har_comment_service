package repo

import "time"

type CollectedApartment struct {
	Id          int64
	UserId      int64
	ApartmentId int64
	CreatedAt   time.Time
}

type AddApartmentReq struct {
	UserId      int64
	ApartmentId int64
}

type GetAllCollectedApartmentsReq struct {
	Page        int64
	Limit       int64
	UserId      int64
	SortByDate  string
	SortByPrice string
}

type GetAllCollectedApartmentsRes struct {
	Apartments []*CollectedApartment
	Count      int64
}

type CollectedApartmentsStorageI interface {
	AddApartment(*AddApartmentReq) (*CollectedApartment, error)
	GetApartment(int64) (*CollectedApartment, error)
	GetAllCollectedApartments(*GetAllCollectedApartmentsReq) (*GetAllCollectedApartmentsRes, error)
	DeleteApartment(int64) error
}
