CREATE TABLE "comments"(
    "id" serial primary key,
    "comment_type" VARCHAR(255) NOT NULL,
    "guest_id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "description" VARCHAR(255),
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);