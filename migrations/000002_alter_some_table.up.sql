CREATE TABLE "likes"(
    "id" serial primary key,
    "user_id" INTEGER NOT NULL,
    "guest_id" INTEGER NOT NULL,
    "status" BOOLEAN NOT NULL,
    "like_type" VARCHAR(255) NOT NULL
);