CREATE TABLE "collected_apartments"(
    "id" serial primary key,
    "user_id" INTEGER NOT NULL,
    "apartment_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);