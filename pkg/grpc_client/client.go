package grpc_client

import (
	"fmt"

	"gitlab.com/har_clone/har_comment_service/config"
	pbb "gitlab.com/har_clone/har_comment_service/genproto/broker_service"
	pbp "gitlab.com/har_clone/har_comment_service/genproto/post_service"
	pbu "gitlab.com/har_clone/har_comment_service/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	ApartmentService() pbp.ApartmentServiceClient
	BrokerService() pbb.BrokerServiceClient
	UserService() pbu.UserServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connBrokerService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.BrokerServiceHost, cfg.BrokerServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("broker service dial host: %s port:%s err: %v",
			cfg.BrokerServiceHost, cfg.BrokerServiceGrpcPort, err)
	}

	connPostService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApartmentServiceHost, cfg.ApartmentServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("post service dial host: %s port:%s err: %v",
			cfg.ApartmentServiceHost, cfg.ApartmentServiceGrpcPort, err)
	}

	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port:%s err: %v",
			cfg.UserServiceHost, cfg.UserServiceGrpcPort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"broker_service":    pbb.NewBrokerServiceClient(connBrokerService),
			"apartment_service": pbp.NewApartmentServiceClient(connPostService),
			"user_service":      pbu.NewUserServiceClient(connUserService),
		},
	}, nil
}

func (g *GrpcClient) BrokerService() pbb.BrokerServiceClient {
	return g.connections["broker_service"].(pbb.BrokerServiceClient)
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) ApartmentService() pbp.ApartmentServiceClient {
	return g.connections["apartment_service"].(pbp.ApartmentServiceClient)
}
