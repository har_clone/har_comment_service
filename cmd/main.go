package main

import (
	"fmt"
	"log"
	"net"

	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	pb "gitlab.com/har_clone/har_comment_service/genproto/comment_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/har_clone/har_comment_service/config"
	"gitlab.com/har_clone/har_comment_service/service"
	"gitlab.com/har_clone/har_comment_service/storage"
)

func main() {
	cfg := config.Load(".")
	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)
	
	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}


	strg := storage.NewStoragePg(psqlConn)
	commentService := service.NewCommentService(strg)
	likeService := service.NewLikeService(strg)
	collectedApartmentsService := service.NewCollectedApartmentsService(strg)

	lis, err := net.Listen("tcp", cfg.GrpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	reflection.Register(s)

	pb.RegisterCommentServiceServer(s, commentService)
	pb.RegisterLikeServiceServer(s, likeService)
	pb.RegisterCollectedApartmentsServiceServer(s, collectedApartmentsService)

	log.Println("Grpc server started in port ", cfg.GrpcPort)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}

}
