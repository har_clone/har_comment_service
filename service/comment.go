package service

import (
	"context"
	"time"

	pb "gitlab.com/har_clone/har_comment_service/genproto/comment_service"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/har_clone/har_comment_service/storage"
)

func parseComment(comment *repo.Comment) *pb.Comment {
	return &pb.Comment{
		Id:          int64(comment.Id),
		GuestId:     int64(comment.GuestId),
		CommentType: comment.CommentType,
		UserId:      int64(comment.UserId),
		Description: comment.Description,
		CreatedAt:   comment.CreatedAt.Format(time.RFC3339),
	}
}

type CommentService struct {
	pb.UnimplementedCommentServiceServer
	storage storage.StorageI
}

func NewCommentService(strg storage.StorageI) *CommentService {
	return &CommentService{
		UnimplementedCommentServiceServer: pb.UnimplementedCommentServiceServer{},
		storage:                           strg,
	}
}

func (s *CommentService) CreateComment(ctx context.Context, req *pb.CommentGenerate) (*pb.Comment, error) {
	comment, err := s.storage.Comment().CreateComment(&repo.CommentCreate{
		GuestId:     req.GuestId,
		UserId:      req.UserId,
		CommentType: req.CommentType,
		Description: req.Description,
	})

	if err != nil {
		return &pb.Comment{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseComment(comment), nil
}

func (s *CommentService) GetComment(ctx context.Context, req *pb.IdByCommentRequest) (*pb.Comment, error) {
	Comment, err := s.storage.Comment().GetComment(req.Id)
	if err != nil {
		return &pb.Comment{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseComment(Comment), nil
}

func (s *CommentService) GetAllComments(ctx context.Context, req *pb.GetCommentRequest) (*pb.GetCommentResponse, error) {
	result, err := s.storage.Comment().GetAllComments(&repo.GetAllCommentRequest{
		Limit:       int(req.Limit),
		Page:        int(req.Page),
		UserId:      int(req.UserId),
		GuestId:     int(req.GuestId),
		Search:      req.Search,
		CommentType: req.CommentType,
		SortByDate:  req.SortByDate,
	})
	if err != nil {
		return &pb.GetCommentResponse{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetCommentResponse{
		Count:    int64(result.Count),
		Comments: make([]*pb.Comment, 0),
	}

	for _, Comment := range result.Comments {
		response.Comments = append(response.Comments, parseComment(Comment))
	}

	return &response, nil
}

func (s *CommentService) UpdateComment(ctx context.Context, req *pb.ChangeComment) (*pb.Comment, error) {
	Comment, err := s.storage.Comment().UpdateComment(&repo.ChangeComment{
		Id:          req.Id,
		Description: req.Description,
		UserId:      req.UserId,
	})

	if err != nil {
		return &pb.Comment{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseComment(Comment), nil
}

func (s *CommentService) DeleteComment(ctx context.Context, req *pb.IdByCommentRequest) (*emptypb.Empty, error) {
	err := s.storage.Comment().DeleteComment(req.Id)
	if err != nil {
		return &emptypb.Empty{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	return &emptypb.Empty{}, nil
}
