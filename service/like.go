package service

import (
	"context"

	pb "gitlab.com/har_clone/har_comment_service/genproto/comment_service"
	"gitlab.com/har_clone/har_comment_service/storage"
	"gitlab.com/har_clone/har_comment_service/storage/repo"
	"google.golang.org/protobuf/types/known/emptypb"
)

type LikeService struct {
	pb.UnimplementedLikeServiceServer
	storage storage.StorageI
}

func NewLikeService(strg storage.StorageI) *LikeService {
	return &LikeService{
		UnimplementedLikeServiceServer: pb.UnimplementedLikeServiceServer{},
		storage:                        strg,
	}
}

func (l *LikeService) CreateOrUpdate(ctx context.Context, req *pb.Like) (*emptypb.Empty, error) {
	err := l.storage.Like().CreateOrUpdate(&repo.Like{
		UserId:   req.UserId,
		LikeType: req.LikeType,
		GuestId:  req.GuestId,
		Status:   req.Status,
	})
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (l *LikeService) Get(ctx context.Context, req *pb.GetLikeInfo) (*pb.Like, error) {
	resp, err := l.storage.Like().Get(&repo.GetLikeInfo{
		LikeType: req.LikeType,
		UserId:   req.UserId,
		GuestId:  req.GuestId,
	})
	if err != nil {
		return &pb.Like{}, err
	}
	return &pb.Like{
		Id:       resp.Id,
		LikeType: req.LikeType,
		GuestId:  resp.GuestId,
		UserId:   resp.UserId,
		Status:   resp.Status,
	}, nil
}

func (l *LikeService) GetLikesDislikesCount(ctx context.Context, req *pb.IdLikeRequest) (*pb.LikesDislikesCountsResult, error) {
	res, err := l.storage.Like().GetLikesDislikesCount(&repo.GetLikesDislikesCountReq{
		Id:       req.Id,
		LikeType: req.LikeType,
	})

	if err != nil {
		return &pb.LikesDislikesCountsResult{}, err
	}

	return &pb.LikesDislikesCountsResult{
		LikesCount:    res.LikesCount,
		DislikesCount: res.DislikesCount,
	}, nil
}
