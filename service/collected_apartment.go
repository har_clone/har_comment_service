package service

import (
	"context"
	"time"

	pb "gitlab.com/har_clone/har_comment_service/genproto/comment_service"
	"gitlab.com/har_clone/har_comment_service/storage/repo"

	"gitlab.com/har_clone/har_comment_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CollectedApartmentsService struct {
	pb.UnimplementedCollectedApartmentsServiceServer
	storage storage.StorageI
}

func NewCollectedApartmentsService(strg storage.StorageI) *CollectedApartmentsService {
	return &CollectedApartmentsService{
		UnimplementedCollectedApartmentsServiceServer: pb.UnimplementedCollectedApartmentsServiceServer{},
		storage: strg,
	}
}

func (s *CollectedApartmentsService) AddApartment(ctx context.Context, req *pb.AddApartmentReq) (*pb.CollectedApartment, error) {
	collectedApartments, err := s.storage.CollectedApartments().AddApartment(&repo.AddApartmentReq{
		UserId:      req.UserId,
		ApartmentId: req.ApartmentId,
	})
	if err != nil {
		return &pb.CollectedApartment{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &pb.CollectedApartment{
		Id:          collectedApartments.Id,
		UserId:      collectedApartments.UserId,
		ApartmentId: collectedApartments.ApartmentId,
		CreatedAt:   collectedApartments.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *CollectedApartmentsService) GetCollectedApartments(ctx context.Context, req *pb.GetApartmentIdRequest) (*pb.CollectedApartment, error) {
	CollectedApartments, err := s.storage.CollectedApartments().GetApartment(req.Id)
	if err != nil {
		return &pb.CollectedApartment{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &pb.CollectedApartment{
		Id:          CollectedApartments.Id,
		UserId:      CollectedApartments.UserId,
		ApartmentId: CollectedApartments.ApartmentId,
		CreatedAt:   CollectedApartments.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *CollectedApartmentsService) GetAllCollectedApartments(ctx context.Context, req *pb.GetAllCollectedApartmentsReq) (*pb.GetAllCollectedApartmentsRes, error) {
	result, err := s.storage.CollectedApartments().GetAllCollectedApartments(&repo.GetAllCollectedApartmentsReq{
		Limit:      req.Limit,
		Page:       req.Page,
		UserId:     req.UserId,
		SortByDate: req.SortByDate,
	})
	if err != nil {
		return &pb.GetAllCollectedApartmentsRes{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllCollectedApartmentsRes{
		Count:      int64(result.Count),
		Apartments: make([]*pb.CollectedApartment, 0),
	}

	for _, CollectedApartments := range result.Apartments {
		response.Apartments = append(response.Apartments, &pb.CollectedApartment{
			Id:          CollectedApartments.Id,
			UserId:      CollectedApartments.UserId,
			ApartmentId: CollectedApartments.ApartmentId,
			CreatedAt:   CollectedApartments.CreatedAt.Format(time.RFC3339),
		})
	}

	return &response, nil
}

func (s *CollectedApartmentsService) DeleteApartment(ctx context.Context, req *pb.GetApartmentIdRequest) (*emptypb.Empty, error) {
	err := s.storage.CollectedApartments().DeleteApartment(req.Id)
	if err != nil {
		return &emptypb.Empty{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	return &emptypb.Empty{}, nil
}
